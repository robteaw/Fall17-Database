package sample;


import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class countryController {

    @FXML
    public void initialize() {
        countryidCol.setCellValueFactory(new PropertyValueFactory<country, String>("CountryID"));
        cnameCol.setCellValueFactory(new PropertyValueFactory<country, String>("CountryName"));
        cnameCol1.setCellValueFactory(new PropertyValueFactory<country, String>("CountryCode"));

        LoadCountries();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void locationAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("location.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<country> tableView;

    @FXML
    private TableColumn<country, String> countryidCol;

    @FXML
    private TableColumn<country, String> cnameCol;

    @FXML
    private TableColumn<country, String> cnameCol1;


    //TextField
    @FXML
    private TextField cnameText;

    @FXML
    private TextField cocoText;


    //Label
    @FXML
    private Label firstView;

    @FXML
    private Label middleView;

    @FXML
    private Label lastView;

    @FXML
    private Label bdateView;

    @FXML
    private Label addView;

    @FXML
    private Label add2View;

    @FXML
    private Label emailView;

    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String countryName = cnameText.getText();
        String countryCode = cocoText.getText();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO Country (CountryName, CountryCode)" + " VALUES (?, ?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , countryName);
            preparedStmt.setString(2 , countryCode);

            preparedStmt.execute();
            tableView.getItems().clear();
            LoadCountries();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add country...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        country cou = tableView.getSelectionModel().getSelectedItem();
        ObservableList<country> cou_List = tableView.getItems();
        cou_List.remove(cou);
        int pk = cou.getCountryID();
        removeCountry(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String countryName = cnameText.getText();
        String countryCode = cocoText.getText();

        country cou = tableView.getSelectionModel().getSelectedItem();
        int pk = cou.getCountryID();
        updateCountry(pk, countryName, countryCode);

    }

    public void updateCountry(int pk, String countryName, String countryCode) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE Country SET CountryName=?, CountryCode=? WHERE CountryID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, countryName);
            stmt.setString(2, countryCode);
            stmt.setInt(3, pk);

            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadCountries();
        } catch (Exception e) {
            System.out.println("opps");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static country searchAction(String countryID) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM country WHERE CountryID=" + countryID;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsCou = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getCountryFromResultSet method and get country object
            country country = getCountryFromResultSet(rsCou);
            System.out.println(country.getCountryName());

            //Return country object
            return country;
        } catch (SQLException e) {
            System.out.println("While searching a country with " + countryID + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM Country");

            while (rs.next()) {
                int countryID = rs.getInt("CountryID");
                listOfKeys.add(new Integer(countryID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add country...");
        }
        return -1;
    }

    //Remove Country
    public void removeCountry(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Country WHERE CountryID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete country...");
        }

    }


    //Use ResultSet from DB as parameter and set Country Object's attributes and return country object.
    private static country getCountryFromResultSet(ResultSet rs) throws SQLException {
        country cou = null;
        if (rs.next()) {
            int countryID = rs.getInt("CountryID");
            String countryName = rs.getString("CountryName");
            String countryCode = rs.getString("CountryCode");
            cou = new country(countryID, countryName, countryCode);
        }
        return cou;
    }


    private void LoadCountries() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Country");


            while (rs.next()) {
                country cou;
                int countryID = rs.getInt("CountryID");
                String countryName = rs.getString("CountryName");
                String countryCode = rs.getString("CountryCode");
                cou = new country(countryID, countryName, countryCode);
                //Add country to the ObservableList

                tableView.getItems().addAll(cou);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Countries couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            country cou = tableView.getSelectionModel().getSelectedItem();
            String countryName = cou.getCountryName();
            String countryCode = cou.getCountryCode();


            cnameText.setText(countryName);
            cocoText.setText(countryCode);

        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        cnameText.setText("");
        cocoText.setText("");

    }
}
