package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class state{

    private int StateID;
    private SimpleStringProperty StateName;
    private SimpleStringProperty StateCode;


    public state(Integer CountyID, String StateName, String StateCode)
    {
        this.StateID = CountyID;
        this.StateName = new SimpleStringProperty(StateName);
        this.StateCode = new SimpleStringProperty(StateCode);

    }
    //StateID
    public Integer getStateID() {
        return StateID;
    }

    public void setStateID(Integer StateID) {
        this.StateID = StateID;
    }

    //StateyName
    public String getStateName() {
        return StateName.get();
    }

    public StringProperty StateProperty() {
        return StateName;
    }

    public void setStateName(String StateName) {
        this.StateName.set(StateName);
    }

    //StateCode
    public String getStateCode() {
        return StateCode.get();
    }

    public StringProperty StateCodeProperty() {
        return StateCode;
    }

    public void setStateCode(String StateCode) {
        this.StateCode.set(StateCode);
    }
}
