package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class response{

    private int ResponseID;
    private SimpleStringProperty ResPhoneNum, ResEmail;
    private Date ResDateTime;


    public response(Integer ResponseID, String ResPhoneNum, String ResEmail, Date ResDateTime)
    {
        this.ResponseID = ResponseID;
        this.ResPhoneNum = new SimpleStringProperty(ResPhoneNum);
        this.ResEmail = new SimpleStringProperty(ResEmail);
        this.ResDateTime = ResDateTime;
    }

    public Integer getResponseID() {
        return ResponseID;
    }

    public void setResponseID(Integer ResponseID) {
        this.ResponseID = ResponseID;
    }

    public String getResPhoneNum() {
        return ResPhoneNum.get();
    }

    public StringProperty phoneProperty() {
        return ResPhoneNum;
    }

    public void setResPhoneNum(String ResPhoneNum) {
        this.ResPhoneNum.set(ResPhoneNum);
    }

    public String getResEmail() {
        return ResEmail.get();
    }

    public StringProperty emailProperty() {
        return ResEmail;
    }

    public void setResEmail(String ResEmail) {
        this.ResEmail.set(ResEmail);
    }

    public Date getResDateTime() {
        return ResDateTime;
    }
    public void setResDateTime(Date ResDateTime) {
        this.ResDateTime = ResDateTime;
    }
}
