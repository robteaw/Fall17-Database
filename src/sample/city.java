package sample;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class city {

    private int CityID;
    private SimpleStringProperty CityName;

    public city(Integer CityID, String CityName) {
        this.CityID = CityID;
        this.CityName = new SimpleStringProperty(CityName);
    }

    //CityID
    public Integer getCityID() {
        return CityID;
    }

    public void setCityID(Integer CityID) {
        this.CityID = CityID;
    }

    //CityName
    public String getCityName() {
        return CityName.get();
    }

    public StringProperty CityProperty() {
        return CityName;
    }

    public void setCityName(String CityName) {
        this.CityName.set(CityName);
    }
}