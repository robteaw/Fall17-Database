package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.Date;

public class inquiryController {


    @FXML
    public void initialize() {
        inquirCol.setCellValueFactory(new PropertyValueFactory<inquiry, String>("InquiryID"));
        dateCol.setCellValueFactory(new PropertyValueFactory<inquiry, Date>("InquiryDateTime"));
        noteCol.setCellValueFactory(new PropertyValueFactory<inquiry, String>("InquirNotes"));

        LoadInquires();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<inquiry> tableView;

    @FXML
    private TableColumn<inquiry, String> inquirCol;

    @FXML
    private TableColumn<inquiry, String> noteCol;

    @FXML
    private TableColumn<inquiry, Date> dateCol;



    //TextField
    @FXML
    private TextField searchText;


    @FXML
    private TextField notesText;

    @FXML
    private DatePicker datetimeText;


    //Buttons
    @FXML
    void addAction(ActionEvent event) {
    }


    @FXML
    void deleteAction(ActionEvent event) {
        inquiry inq = tableView.getSelectionModel().getSelectedItem();
        ObservableList<inquiry> inq_List = tableView.getItems();
        inq_List.remove(inq);
        int pk = inq.getInquiryID();
        removeInquiry(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String notes = notesText.getText();

        inquiry inq = tableView.getSelectionModel().getSelectedItem();
        int pk = inq.getInquiryID();
        updateInquiry(pk, notes);

    }

    public void updateInquiry(int pk, String notes){
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE Inquiry SET InquirNotes=?, WHERE InquiryID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, notes);
            stmt.setInt(2, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadInquires();
        }
        catch (Exception e){
            System.out.println("opps");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }


    //Remove Inquiry
    public void removeInquiry(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Inquiry WHERE InquiryID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete inquiry...");
        }

    }


    //Use ResultSet from DB as parameter and set Inquiry Object's attributes and return inquiry object.
    private static inquiry getInquiryFromResultSet(ResultSet rs) throws SQLException {
        inquiry inq = null;
        if (rs.next()) {
            int inqID = rs.getInt("InquiryID");
            Date datetime = rs.getDate("InquiryDateTime");
            String notes = rs.getString("InquirNotes");

            inq = new inquiry(inqID, notes, datetime);
        }
        return inq;
    }


    private void LoadInquires() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Inquiry");


            while (rs.next()) {
                inquiry inq;
                int inqID = rs.getInt("InquiryID");
                Date datetime = rs.getDate("InquiryDateTime");
                String notes = rs.getString("InquirNotes");

                inq = new inquiry(inqID, notes, datetime);
                //Add inquiry to the ObservableList

                tableView.getItems().addAll(inq);
                tableView.refresh();
                System.out.println(tableView.getItems());
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Inquires couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            inquiry inq = tableView.getSelectionModel().getSelectedItem();
            String notes = inq.getInquirNotes();

            notesText.setText(notes);
        });

    }


    //Clear Text Field
    public void clearAction(ActionEvent event) {
        //datetimeText.setValue(null);
        notesText.setText("");

    }
}