package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class client{

    private int ClientID;
    private SimpleStringProperty ClientFName, ClientLName;
    private Date ClientBDate;


    public client(Integer ClientID, String ClientFName, String ClientLName, Date ClientBdate)
    {
        this.ClientID = ClientID;
        this.ClientFName = new SimpleStringProperty(ClientFName);
        this.ClientLName = new SimpleStringProperty(ClientLName);
        this.ClientBDate = ClientBDate;
    }

    public Integer getClientID() {
        return ClientID;
    }

    public void setClientID(Integer ClientID) {
        this.ClientID = ClientID;
    }

    public String getClientFName() {
        return ClientFName.get();
    }

    public StringProperty firstProperty() {
        return ClientFName;
    }

    public void setClientFName(String ClientFName) {
        this.ClientFName.set(ClientFName);
    }

    public String getClientLName() {
        return ClientLName.get();
    }

    public StringProperty middleProperty() {
        return ClientLName;
    }

    public void setClientLName(String ClientLName) {
        this.ClientLName.set(ClientLName);
    }

    public Date getClientBDate() {
        return ClientBDate;
    }
    public void setClientBDate(Date EmpBdate) {
        this.ClientBDate = ClientBDate;
    }
}
