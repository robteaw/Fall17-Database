package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class country{

    private int CountryID;
    private SimpleStringProperty CountryName;
    private SimpleStringProperty CountryCode;

    public country(Integer CountryID, String CountryName, String CountryCode)
    {
        this.CountryID = CountryID;
        this.CountryName = new SimpleStringProperty(CountryName);
        this.CountryCode = new SimpleStringProperty(CountryCode);
    }
    //CountryID
    public Integer getCountryID() {
        return CountryID;
    }

    public void setCountryID(Integer CountryID) {
        this.CountryID = CountryID;
    }

    //CountryName
    public String getCountryName() {
        return CountryName.get();
    }

    public StringProperty CountryProperty() {
        return CountryName;
    }

    public void setCountryName(String CountryName) {
        this.CountryName.set(CountryName);
    }

    //CountryCode
    public String getCountryCode() {
        return CountryCode.get();
    }

    public StringProperty CountryCodeProperty() {
        return CountryCode;
    }

    public void setCountryCode(String CountryCode) {
        this.CountryCode.set(CountryCode);
    }
}
