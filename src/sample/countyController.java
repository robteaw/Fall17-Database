package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class countyController {


    @FXML
    public void initialize() {
        countidCol.setCellValueFactory(new PropertyValueFactory<county, String>("CountyID"));
        cnameCol.setCellValueFactory(new PropertyValueFactory<county, String>("CountyName"));

        LoadCounties();

    }

    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void locationAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("location.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<county> tableView;

    @FXML
    private TableColumn<county, String> countidCol;

    @FXML
    private TableColumn<county, String> cnameCol;



    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField countyText;


    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String countyName = countyText.getText();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO County (CountyName)" + " VALUES (?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , countyName);

            preparedStmt.execute();
            tableView.getItems().clear();
            LoadCounties();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add county...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        county cou = tableView.getSelectionModel().getSelectedItem();
        ObservableList<county> cou_List = tableView.getItems();
        cou_List.remove(cou);
        int pk = cou.getCountyID();
        removeCounty(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String countyName = countyText.getText();

        county cou = tableView.getSelectionModel().getSelectedItem();
        int pk = cou.getCountyID();
        updateCounty(pk, countyName);

    }

    public void updateCounty(int pk, String countyName) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE County SET CountyName=? WHERE CountyID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, countyName);
            stmt.setInt(2, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadCounties();
        } catch (Exception e) {
            System.out.println("oops");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static county searchAction(String countyId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM county WHERE CountyID=" + countyId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsCou = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getCountyFromResultSet method and get county object
            county county = getCountyFromResultSet(rsCou);
            System.out.println(county.getCountyName());

            //Return county object
            return county;
        } catch (SQLException e) {
            System.out.println("While searching an county with " + countyId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM County");

            while (rs.next()) {
                int countyID = rs.getInt("CountyID");
                listOfKeys.add(new Integer(countyID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add county...");
        }
        return -1;
    }

    //Remove County
    public void removeCounty(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM County WHERE CountyID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete county...");
        }

    }


    //Use ResultSet from DB as parameter and set County Object's attributes and return county object.
    private static county getCountyFromResultSet(ResultSet rs) throws SQLException {
        county cou = null;
        if (rs.next()) {
            int countyID = rs.getInt("CountyID");
            String countyName = rs.getString("ECountyName");
            cou = new county(countyID, countyName);
        }
        return cou;
    }


    private void LoadCounties() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM County");


            while (rs.next()) {
                county cou;
                int countyID = rs.getInt("CountyID");
                String countyName = rs.getString("CountyName");
                cou = new county(countyID, countyName);
                //Add county to the ObservableList

                tableView.getItems().addAll(cou);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Counties couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            county cou = tableView.getSelectionModel().getSelectedItem();
            String countyName = cou.getCountyName();


            countyText.setText(countyName);
        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        countyText.setText("");

    }
}
