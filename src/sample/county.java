package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class county{

    private int CountyID;
    private SimpleStringProperty CountyName;

    public county(Integer CountyID, String CountyName)
    {
        this.CountyID = CountyID;
        this.CountyName = new SimpleStringProperty(CountyName);
    }
    //CountyID
    public Integer getCountyID() {
        return CountyID;
    }

    public void setCountyID(Integer CountyID) {
        this.CountyID = CountyID;
    }

    //CountyName
    public String getCountyName() {
        return CountyName.get();
    }

    public StringProperty CountyProperty() {
        return CountyName;
    }

    public void setCountyName(String CountyName) {
        this.CountyName.set(CountyName);
    }
}
