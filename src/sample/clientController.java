package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.GregorianCalendar;

public class clientController {


    @FXML
    public void initialize() {
        clientidCol.setCellValueFactory(new PropertyValueFactory<client, String>("ClientID"));
        first1Col.setCellValueFactory(new PropertyValueFactory<client, String>("ClientFName"));

        last1Col.setCellValueFactory(new PropertyValueFactory<client, String>("ClientLName"));
        bdate1Col.setCellValueFactory(new PropertyValueFactory<client, Date>("EmpBdate"));


        LoadClients();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<client> tableView;

    @FXML
    private TableColumn<client, String> clientidCol;

    @FXML
    private TableColumn<client, String> first1Col;

    @FXML
    private TableColumn<client, String> last1Col;

    @FXML
    private TableColumn<client, Date> bdate1Col;

    @FXML
    private TableColumn<client, String> addCol;

    @FXML
    private TableColumn<client, String> add2Col;

    @FXML
    private TableColumn<client, String> emailCol;

    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField first1Text;



    @FXML
    private TextField last1Text;

    @FXML
    private DatePicker bdate1Text;

    //Label
    @FXML
    private Label firstView;

    @FXML
    private Label middleView;

    @FXML
    private Label lastView;

    @FXML
    private Label bdateView;

    @FXML
    private Label addView;

    @FXML
    private Label add2View;

    @FXML
    private Label emailView;

    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String firstName = first1Text.getText();

        String lastName = last1Text.getText();

        LocalDate localDate = bdate1Text.getValue();
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        GregorianCalendar gc = new GregorianCalendar(year, month-1, day);
        long dateTime = gc.getTimeInMillis();
        Date date = new Date(dateTime);

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO Client (ClientFName, ClientLName, ClientBDate, GenderID)" + " VALUES (?, ?, ?, ?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , firstName);

            preparedStmt.setString(2 , lastName);
            preparedStmt.setDate(3 , date);

            preparedStmt.setString(4 , null);


            preparedStmt.execute();
            tableView.getItems().clear();
            LoadClients();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add client...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        client cli = tableView.getSelectionModel().getSelectedItem();
        ObservableList<client> cli_List = tableView.getItems();
        cli_List.remove(cli);
        int pk = cli.getClientID();
        removeClient(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String firstName = first1Text.getText();

        String lastName = last1Text.getText();



        client cli = tableView.getSelectionModel().getSelectedItem();
        int pk = cli.getClientID();
        updateClient(pk, firstName, lastName);

    }

    public void updateClient(int pk, String firstName, String lastName) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE Client SET ClientFName=?, ClientLName=? WHERE ClientID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, firstName);
            stmt.setString(2, lastName);
            stmt.setInt(3, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadClients();
        } catch (Exception e) {
            System.out.println("opps");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static client searchAction(String clientId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM client WHERE ClientID=" + clientId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getClientFromResultSet method and get client object
            client client = getClientFromResultSet(rsEmp);
            System.out.println(client.getClientFName());

            //Return client object
            return client;
        } catch (SQLException e) {
            System.out.println("While searching an client with " + clientId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM Client");

            while (rs.next()) {
                int clientID = rs.getInt("ClientID");
                listOfKeys.add(new Integer(clientID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add client...");
        }
        return -1;
    }

    //Remove Client
    public void removeClient(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Client WHERE ClientID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete client...");
        }

    }


    //Use ResultSet from DB as parameter and set Client Object's attributes and return client object.
    private static client getClientFromResultSet(ResultSet rs) throws SQLException {
        client cli = null;
        if (rs.next()) {
            int clientID = rs.getInt("ClientID");
            String firstName = rs.getString("ClientFName");

            String lastName = rs.getString("ClientLName");
            Date dateBDay = rs.getDate("ClientBDate");

            cli = new client(clientID, firstName, lastName, dateBDay);
        }
        return cli;
    }


    private void LoadClients() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Client");


            while (rs.next()) {
                client cli;
                int clientID = rs.getInt("ClientID");
                String firstName = rs.getString("ClientFName");

                String lastName = rs.getString("ClientLName");
                Date dateBDay = rs.getDate("ClientBDate");

                cli = new client(clientID, firstName, lastName, dateBDay);
                //Add client to the ObservableList

                tableView.getItems().addAll(cli);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Clients couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            client cli = tableView.getSelectionModel().getSelectedItem();
            String firstName = cli.getClientFName();

            String lastName = cli.getClientLName();


            first1Text.setText(firstName);

            last1Text.setText(lastName);

        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        first1Text.setText("");

        last1Text.setText("");
        bdate1Text.setValue(null);

    }
}
