package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class employee{

   private int EmpID;
   private SimpleStringProperty EmpFName, EmpMName, EmpLName, EmpAddress1, EmpAddress2, EmpEmail;
   private Date EmpBdate;


   public employee(Integer EmpID, String EmpFName, String EmpMName, String EmpLName, Date EmpBdate, String EmpAddress1, String EmpAddress2, String EmpEmail)
   {
   this.EmpID = EmpID;
   this.EmpFName = new SimpleStringProperty(EmpFName);
   this.EmpMName = new SimpleStringProperty(EmpMName);
   this.EmpLName = new SimpleStringProperty((EmpLName));
   this.EmpBdate = EmpBdate;
   this.EmpAddress1 = new SimpleStringProperty(EmpAddress1);
   this.EmpAddress2 = new SimpleStringProperty(EmpAddress2);
   this.EmpEmail = new SimpleStringProperty(EmpEmail);
   }

    public Integer getEmpID() {
        return EmpID;
    }

    public void setEmpID(Integer EmpID) {
        this.EmpID = EmpID;
    }

    public String getEmpFName() {
        return EmpFName.get();
    }

    public StringProperty firstProperty() {
        return EmpFName;
    }

    public void setEmpFName(String EmpFName) {
        this.EmpFName.set(EmpFName);
    }

    public StringProperty getEmpFNameProperty(){
       return this.EmpFName;
    }

    public String getEmpMName() {
        return EmpMName.get();
    }

    public StringProperty middleProperty() {
        return EmpMName;
    }

    public void setEmpMName(String EmpMName) {
        this.EmpMName.set(EmpMName);
    }

    public String getEmpLName() {
        return EmpLName.get();
    }

    public StringProperty lastProperty() {
        return EmpLName;
    }

    public void setEmpLName(String EmpLName) {
        this.EmpLName.set(EmpLName);
    }

    public String getEmpAddress1() {
        return EmpAddress1.get();
    }

    public StringProperty addProperty() {
        return EmpAddress1;
    }

    public void setEmpAddress1(String EmpAddress1) {
        this.EmpAddress1.set(EmpAddress1);
    }

    public String getEmpAddress2() {
        return EmpAddress2.get();
    }

    public StringProperty add2Property() {
        return EmpAddress2;
    }

    public void setEmpAddress2(String EmpAddress2) {
        this.EmpAddress2.set(EmpAddress2);
    }

    public String getEmpEmail() {
        return EmpEmail.get();
    }

    public StringProperty emailProperty() {
        return EmpEmail;
    }

    public void setEmpEmail(String EmpEmail) {
        this.EmpEmail.set(EmpEmail);
    }

    public Date getEmpBdate() {
        return EmpBdate;
    }
    public void setEmpBdate(Date EmpBdate) {
        this.EmpBdate = EmpBdate;
    }
}
