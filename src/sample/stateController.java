/**
package sample;


import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class stateController {

    @FXML
    public void initialize() {
        stateidCol.setCellValueFactory(new PropertyValueFactory<state, String>("StateID"));
        nameCol.setCellValueFactory(new PropertyValueFactory<state, String>("StateName"));
        codeCol.setCellValueFactory(new PropertyValueFactory<state, String>("StateCode"));

        LoadStates();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void locationAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("location.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<state> tableView;

    @FXML
    private TableColumn<state, String> stateidCol;

    @FXML
    private TableColumn<state, String> nameCol;

    @FXML
    private TableColumn<state, String> codeCol;

    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField stateText;

    @FXML
    private TextField codeText;


    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String stateName = stateText.getText();
        String stateCode = codeText.getText();


        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO State (StateName, StateCode)" + " VALUES (?, ?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, stateName);
            preparedStmt.setString(2, stateCode);

            preparedStmt.execute();
            tableView.getItems().clear();
            LoadStates();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add state...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        state sta = tableView.getSelectionModel().getSelectedItem();
        ObservableList<state> sta_List = tableView.getItems();
        sta_List.remove(sta);
        int pk = sta.getStateID();
        removeState(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String stateName = stateText.getText();
        String stateCode = codeText.getText();


        state sta = tableView.getSelectionModel().getSelectedItem();
        int pk = sta.getStateID();
        updateState(pk, stateName, stateCode);

    }

    public void updateState(int pk, String stateName, String stateCode) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE State SET StateName=?, StateCode=? WHERE StateID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, stateName);
            stmt.setString(2, stateCode);
            stmt.setInt(3, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadStates();
        } catch (Exception e) {
            System.out.println("oops");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static state searchAction(String stateId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM state WHERE StateID=" + stateId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsSta = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getStateFromResultSet method and get state object
            state state = getStateFromResultSet(rsSta);
            System.out.println(state.getStateName());

            //Return state object
            return state;
        } catch (SQLException e) {
            System.out.println("While searching an state with " + stateId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM State");

            while (rs.next()) {
                int stateID = rs.getInt("StateID");
                listOfKeys.add(new Integer(stateID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add state...");
        }
        return -1;
    }

    //Remove State
    public void removeState(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM State WHERE StateID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete state...");
        }

    }


    //Use ResultSet from DB as parameter and set State Object's attributes and return state object.
    private static state getStateFromResultSet(ResultSet rs) throws SQLException {
        state sta = null;
        if (rs.next()) {
            int stateID = rs.getInt("StateID");
            String stateName = rs.getString("StateName");
            String stateCode = rs.getString("StateCode");

            sta = new state(stateID, stateName, stateCode);
        }
        return sta;
    }


    private void LoadStates() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM State");


            while (rs.next()) {
                state sta;
                int stateID = rs.getInt("StateID");
                String stateName = rs.getString("StateName");
                String stateCode = rs.getString("StateCode");

                sta = new state(stateID, stateName, stateCode);
                //Add state to the ObservableList

                tableView.getItems().addAll(sta);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("States couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            state sta = tableView.getSelectionModel().getSelectedItem();
            String stateName = sta.getStateName();


           stateText.setText(stateName);
        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        stateText.setText("");
        codeText.setText("");

    }
}

***/