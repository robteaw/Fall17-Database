package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.GregorianCalendar;

public class appointmentController {


    @FXML
    public void initialize() {
        appointCol.setCellValueFactory(new PropertyValueFactory<appointment, String>("AppointID"));
        dateCol.setCellValueFactory(new PropertyValueFactory<appointment, Date>("AppointDateTime"));

        LoadAppointments();

    }

    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<appointment> tableView;

    @FXML
    private TableColumn<appointment, String> appointCol;

    @FXML
    private TableColumn<appointment, Date> dateCol;



    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private DatePicker dateText;


    //Label
    @FXML
    private Label appointView;

    @FXML
    private Label dateView;


    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        LocalDate localDate = dateText.getValue();
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        GregorianCalendar gc = new GregorianCalendar(year, month-1, day);
        long dateTime = gc.getTimeInMillis();
        Date appointDate = new Date(dateTime);

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO Appointment (AppointDateTime)" + " VALUES (?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setDate(1 , appointDate);


            preparedStmt.execute();
            tableView.getItems().clear();
            LoadAppointments();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add appointment...");

        }

    }

    @FXML
    void deleteAction(ActionEvent event) {
        appointment app = tableView.getSelectionModel().getSelectedItem();
        ObservableList<appointment> app_List = tableView.getItems();
        app_List.remove(app);
        int pk = app.getAppointID();
        removeAppointment(pk);
    }

    @FXML
    void editAction(ActionEvent event) {




        appointment app = tableView.getSelectionModel().getSelectedItem();
        int pk = app.getAppointID();
        updateAppointment(pk);

    }

    public void updateAppointment(int pk) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            //String sql = "UPDATE Appointment AppointDateTime=? WHERE AppointID=?";

            //PreparedStatement stmt = conn.prepareStatement(sql);
            // stmt.setDate(1 , appointDateTime);

            //stmt.executeUpdate();
            tableView.getItems().clear();
            LoadAppointments();
        } catch (Exception e) {
            System.out.println("opps");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static appointment searchAction(String appointID) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM appointment WHERE AppointmentID=" + appointID;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsApp = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getAppointmentFromResultSet method and get appointment object
            appointment appointment = getAppointmentFromResultSet(rsApp);
            System.out.println(appointment.getAppointDateTime());

            //Return appointment object
            return appointment;
        } catch (SQLException e) {
            System.out.println("While searching an appointment with " + appointID + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM Appointment");

            while (rs.next()) {
                int appointID = rs.getInt("AppointID");
                listOfKeys.add(new Integer(appointID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add appointment...");
        }
        return -1;
    }

    //Remove Appointment
    public void removeAppointment(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Appointment WHERE AppointID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete appointment...");
        }

    }


    //Use ResultSet from DB as parameter and set Appointment Object's attributes and return appointment object.
    private static appointment getAppointmentFromResultSet(ResultSet rs) throws SQLException {
        appointment app = null;
        if (rs.next()) {
            int appointID = rs.getInt("AppointID");
            Date appointDateTime = rs.getDate("AppointDateTime");

            app = new appointment(appointID, appointDateTime);
        }
        return app;
    }


    private void LoadAppointments() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Appointment");


            while (rs.next()) {
                appointment app;
                int appointID = rs.getInt("AppointID");
                Date appointDateTime = rs.getDate("AppointDateTime");
                app = new appointment(appointID, appointDateTime);
                //Add appointment to the ObservableList

                tableView.getItems().addAll(app);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Appointments couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            appointment app = tableView.getSelectionModel().getSelectedItem();

        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {

        dateText.setValue(null);

    }
}
