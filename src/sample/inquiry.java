package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;

public class inquiry{

    private int InquiryID;
    private SimpleStringProperty InquirNotes;
    private Date InquiryDateTime;


    public inquiry(Integer InquiryID, String InquirNotes, Date InquiryDateTime)
    {
        this.InquiryID = InquiryID;
        this.InquirNotes = new SimpleStringProperty(InquirNotes);
        this.InquiryDateTime = InquiryDateTime;
    }

    public Integer getInquiryID() {
        return InquiryID;
    }

    public void setInquiryID(Integer InquiryID) {
        this.InquiryID = InquiryID;
    }

    public String getInquirNotes() {
        return InquirNotes.get();
    }

    public StringProperty notesProperty() {
        return InquirNotes;
    }

    public void setInquirNotes(String InquirNotes) {
        this.InquirNotes.set(InquirNotes);
    }

    public Date getInquiryDateTime() {
        return InquiryDateTime;
    }
    public void setInquiryDateTime(Date InquiryDateTime) {
        this.InquiryDateTime = InquiryDateTime;
    }

}
