package sample;


import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class cityController {

    @FXML
    public void initialize() {
        cityidCol.setCellValueFactory(new PropertyValueFactory<city, String>("CityID"));
        cnameCol.setCellValueFactory(new PropertyValueFactory<city, String>("CityName"));

        LoadCities();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void locationAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("location.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<city> tableView;

    @FXML
    private TableColumn<city, String> cityidCol;

    @FXML
    private TableColumn<city, String> cnameCol;



    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField cityText;

    //Label
    @FXML
    private Label firstView;

    @FXML
    private Label middleView;

    @FXML
    private Label lastView;

    @FXML
    private Label bdateView;

    @FXML
    private Label addView;

    @FXML
    private Label add2View;

    @FXML
    private Label emailView;

    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String cityName = cityText.getText();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO City (CityName)" + " VALUES (?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , cityName);

            preparedStmt.execute();
            tableView.getItems().clear();
            LoadCities();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add city...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        city cou = tableView.getSelectionModel().getSelectedItem();
        ObservableList<city> cou_List = tableView.getItems();
        cou_List.remove(cou);
        int pk = cou.getCityID();
        removeCity(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String cityName = cityText.getText();

        city cou = tableView.getSelectionModel().getSelectedItem();
        int pk = cou.getCityID();
        updateCity(pk, cityName);

    }

    public void updateCity(int pk, String cityName) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE City SET CityName=? WHERE CityID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, cityName);
            stmt.setInt(2, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadCities();
        } catch (Exception e) {
            System.out.println("oops");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {


    }

    public static city searchAction(String cityId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM city WHERE CityID=" + cityId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsCou = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getCityFromResultSet method and get city object
            city city = getCityFromResultSet(rsCou);
            System.out.println(city.getCityName());

            //Return city object
            return city;
        } catch (SQLException e) {
            System.out.println("While searching an city with " + cityId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM City");

            while (rs.next()) {
                int cityID = rs.getInt("CityID");
                listOfKeys.add(new Integer(cityID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add city...");
        }
        return -1;
    }

    //Remove City
    public void removeCity(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM City WHERE CityID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete city...");
        }

    }


    //Use ResultSet from DB as parameter and set City Object's attributes and return city object.
    private static city getCityFromResultSet(ResultSet rs) throws SQLException {
        city cou = null;
        if (rs.next()) {
            int cityID = rs.getInt("CityID");
            String cityName = rs.getString("ECityName");
            cou = new city(cityID, cityName);
        }
        return cou;
    }


    private void LoadCities() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM City");


            while (rs.next()) {
                city cou;
                int cityID = rs.getInt("CityID");
                String cityName = rs.getString("CityName");
                cou = new city(cityID, cityName);
                //Add city to the ObservableList

                tableView.getItems().addAll(cou);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Cities couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            city cou = tableView.getSelectionModel().getSelectedItem();
            String cityName = cou.getCityName();


            cityText.setText(cityName);
        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        cityText.setText("");

    }
}

