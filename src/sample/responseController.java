package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.GregorianCalendar;


public class responseController {


    @FXML
    public void initialize() {
        respidCol.setCellValueFactory(new PropertyValueFactory<response, String>("ResponseID"));
        datetimeCol.setCellValueFactory(new PropertyValueFactory<response, Date>("ResDateTime"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<response, String>("ResPhoneNum"));
        emailCol.setCellValueFactory(new PropertyValueFactory<response, String>("ResEmail"));

        LoadResponse();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<response> tableView1;

    @FXML
    private TableColumn<response, String> respidCol;

    @FXML
    private TableColumn<response, Date> datetimeCol;

    @FXML
    private TableColumn<response, String> phoneCol;

    @FXML
    private TableColumn<response, String> emailCol;

    //TextField
    @FXML
    private DatePicker dateText;

    @FXML
    private TextField phoneText;

    @FXML
    private TextField emailText;

    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String phone = phoneText.getText();
        String email = emailText.getText();
        LocalDate localDate = dateText.getValue();
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        GregorianCalendar gc = new GregorianCalendar(year, month-1, day);
        long dateTime = gc.getTimeInMillis();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO Response (ResPhoneNum, ResEmail, ResOutcomeID, InquiryID, ClientID, EmpID)" + " VALUES (?, ?, ?, ?, ?, ?,)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , phone);
            preparedStmt.setString(2 , email);
            preparedStmt.setString(3 , null);
            preparedStmt.setString(4 , null);
            preparedStmt.setString(5 , null);
            preparedStmt.setString(6 , null);


            preparedStmt.execute();
            tableView1.getItems().clear();
            LoadResponse();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add response...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        response resp = tableView1.getSelectionModel().getSelectedItem();
        ObservableList<response> resp_List = tableView1.getItems();
        resp_List.remove(resp);
        int pk = resp.getResponseID();
        removeResponse(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String phone = phoneText.getText();
        String email = emailText.getText();


        response resp = tableView1.getSelectionModel().getSelectedItem();
        int pk = resp.getResponseID();
        updateEmployee(pk, phone, email);

    }

    public void updateEmployee(int pk, String phone, String email) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE Response SET ResPhoneNum=?, ResEmail=?, WHERE ResponseID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, phone);
            stmt.setString(2, email);
            stmt.setInt(3, pk);
            stmt.executeUpdate();
            tableView1.getItems().clear();
            LoadResponse();
        } catch (Exception e) {
            System.out.println("oops");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {

    }


    public static response searchAction(String respId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM response WHERE ResponseID=" + respId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsResp = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getResponseFromResultSet method and get employee object
            response response = getResponseFromResultSet(rsResp);
            System.out.println(response.getResponseID());

            //Return response object
            return response;
        } catch (SQLException e) {
            System.out.println("While searching an response with " + respId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    //Auto generates an ID
    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM Response");

            while (rs.next()) {
                int respID = rs.getInt("EmpID");
                listOfKeys.add(new Integer(respID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add employee...");
        }
        return -1;
    }

    //Remove Response
    public void removeResponse(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Response WHERE ResponseID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete response...");
        }

    }


    //Use ResultSet from DB as parameter and set Employee Object's attributes and return employee object.
    private static response getResponseFromResultSet(ResultSet rs) throws SQLException {
        response resp = null;
        if (rs.next()) {
            int respID = rs.getInt("ResponseID");
            Date date = rs.getDate("ResDateTime");
            String phone = rs.getString("ResPhoneNum");
            String email = rs.getString("EmpEmail");
            resp = new response(respID, phone, email, date);
        }
        return resp;
    }


    private void LoadResponse() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Response");


            while (rs.next()) {
                response resp;
                int respID = rs.getInt("ResponseID");
                Date date = rs.getDate("ResDateTime");
                String phone = rs.getString("ResPhoneNum");
                String email = rs.getString("ResEmail");
                resp = new response(respID, phone, email, date);
                //Add response to the ObservableList

                tableView1.getItems().addAll(resp);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Response couldn't be loaded...");
        }
        tableView1.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            response resp = tableView1.getSelectionModel().getSelectedItem();
            String phone = resp.getResPhoneNum();
            String email = resp.getResEmail();

            phoneText.setText(phone);
            emailText.setText(email);
        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        dateText.setValue(null);
        phoneText.setText("");
        emailText.setText("");

    }
}