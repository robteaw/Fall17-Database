package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class empTypeController {


    @FXML
    public void initialize() {
        etypeCol.setCellValueFactory(new PropertyValueFactory<empType, String>("EmpTypeID"));
        typeCol.setCellValueFactory(new PropertyValueFactory<empType, String>("EmpType"));
        descrCol.setCellValueFactory(new PropertyValueFactory<empType, String>("EmpTypeDesc"));

        LoadEmpType();

    }


    public void empAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<empType> tableView;

    @FXML
    private TableColumn<empType, String> etypeCol;

    @FXML
    private TableColumn<empType, String> typeCol;

    @FXML
    private TableColumn<empType, String> descrCol;


    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField typeText;

    @FXML
    private TextField descrText;



    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String type = typeText.getText();
        String descr = descrText.getText();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            /*Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://CoT-CIS3365-09.cougarnet.uh.edu;" +
                    "databaseName=PhoenixSolutions;user=;password=;";       */
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO EmpType (EmpType, EmpTypeDesc)" + " VALUES (?, ?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , type);
            preparedStmt.setString(2 , descr);

            preparedStmt.execute();
            tableView.getItems().clear();
            LoadEmpType();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add employee type...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        empType empt = tableView.getSelectionModel().getSelectedItem();
        ObservableList<empType> empt_List = tableView.getItems();
        empt_List.remove(empt);
        int pk = empt.getEmpTypeID();
        removeEmpType(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String type = typeText.getText();
        String descr= descrText.getText();


        empType empt = tableView.getSelectionModel().getSelectedItem();
        int pk = empt.getEmpTypeID();
        updateEmpType(pk, type, descr);

    }

    public void updateEmpType(int pk, String type, String descr) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE EmpType SET EmpType=?, EmpTypeDesc=? WHERE EmpTypeID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, type);
            stmt.setString(2, descr);
            stmt.setInt(3, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadEmpType();
        } catch (Exception e) {
            System.out.println("oops");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {

    }


    public static empType searchAction(String empTypeId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM empType WHERE EmpTypeID=" + empTypeId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getEmployeeFromResultSet method and get employee object
            empType empType = getEmpTypeFromResultSet(rsEmp);
            System.out.println(empType.getEmpType());

            //Return employee type object
            return empType;
        } catch (SQLException e) {
            System.out.println("While searching an employee with " + empTypeId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    //Auto generates an ID
    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM EmpType");

            while (rs.next()) {
                int empTypeID = rs.getInt("EmpTypeID");
                listOfKeys.add(new Integer(empTypeID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add employee type...");
        }
        return -1;
    }

    //Remove Employee TYpe
    public void removeEmpType(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM EmpType WHERE EmpTypeID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete employee type...");
        }

    }


    //Use ResultSet from DB as parameter and set Employee Object's attributes and return employee object.
    private static empType getEmpTypeFromResultSet(ResultSet rs) throws SQLException {
        empType empType = null;
        if (rs.next()) {
            int empTypeID = rs.getInt("EmpTypeID");
            String type = rs.getString("EmpType");
            String descr = rs.getString("EmpTypeDesc");

            empType = new empType(empTypeID, type, descr);
        }
        return empType;
    }


    private void LoadEmpType() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM EmpType");


            while (rs.next()) {
                empType empt;
                int empTypeID = rs.getInt("EmpTypeID");
                String type = rs.getString("EmpType");
                String descr = rs.getString("EmpTypeDesc");

                empt = new empType(empTypeID, type, descr);
                //Add employee type to the ObservableList

                tableView.getItems().addAll(empt);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Employees couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            empType empt = tableView.getSelectionModel().getSelectedItem();
            String type = empt.getEmpType();
            String descr = empt.getEmpTypeDesc();


            typeText.setText(type);
            descrText.setText(descr);

        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        typeText.setText("");
        descrText.setText("");

    }
}
