package sample;

import java.util.Date;

public class appointment{

    private int AppointID;
    private Date AppointDateTime;


    public appointment(Integer AppointID, Date AppointDateTime)
    {
        this.AppointID = AppointID;
        this.AppointDateTime = AppointDateTime;
    }

    public Integer getAppointID() {
        return AppointID;
    }

    public void setAppointID(Integer AppointID) {
        this.AppointID = AppointID;
    }

    public Date getAppointDateTime() {
        return AppointDateTime;
    }
    public void setAppointDateTime(Date AppointDateTime) {
        this.AppointDateTime = AppointDateTime;
    }
}
