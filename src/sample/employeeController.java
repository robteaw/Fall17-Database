package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.GregorianCalendar;

public class employeeController {


    @FXML
    public void initialize() {
        empidCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpID"));
        firstCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpFName"));
        middleCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpMName"));
        lastCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpLName"));
        bdateCol.setCellValueFactory(new PropertyValueFactory<employee, Date>("EmpBdate"));
        addCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpAddress1"));
        add2Col.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpAddress2"));
        emailCol.setCellValueFactory(new PropertyValueFactory<employee, String>("EmpEmail"));

        LoadEmployees();

    }


    public void mainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void emptyAction(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("empType.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((javafx.scene.Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    //Columns
    @FXML
    private TableView<employee> tableView;

    @FXML
    private TableColumn<employee, String> empidCol;

    @FXML
    private TableColumn<employee, String> firstCol;

    @FXML
    private TableColumn<employee, String> middleCol;

    @FXML
    private TableColumn<employee, String> lastCol;

    @FXML
    private TableColumn<employee, Date> bdateCol;

    @FXML
    private TableColumn<employee, String> addCol;

    @FXML
    private TableColumn<employee, String> add2Col;

    @FXML
    private TableColumn<employee, String> emailCol;

    //TextField
    @FXML
    private TextField searchText;

    @FXML
    private TextField firstText;

    @FXML
    private TextField middleText;

    @FXML
    private TextField lastText;

    @FXML
    private TextField addText;

    @FXML
    private TextField add2Text;

    @FXML
    private TextField emailText;

    @FXML
    private DatePicker bdateText;

    //Buttons
    @FXML
    void addAction(ActionEvent event) {

        String firstName = firstText.getText();
        String middleName = middleText.getText();
        String lastName = lastText.getText();
        String email = emailText.getText();
        String add = addText.getText();
        String add2 = add2Text.getText();
        LocalDate localDate = bdateText.getValue();
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        GregorianCalendar gc = new GregorianCalendar(year, month-1, day);
        long dateTime = gc.getTimeInMillis();
        Date date = new Date(dateTime);

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            String query = ("INSERT INTO Employee (EmpFName, EmpMName, EmpLName, EmpBdate, EmpAddress1, EmpAddress2, EmpEmail, LocationID)" + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1 , firstName);
            preparedStmt.setString(2 , middleName);
            preparedStmt.setString(3 , lastName);
            preparedStmt.setDate(4 , date);
            preparedStmt.setString(5 , add);
            preparedStmt.setString(6 , add2);
            preparedStmt.setString(7 , email);
            preparedStmt.setString(8 , null);


            preparedStmt.execute();
            tableView.getItems().clear();
            LoadEmployees();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add employee...");

        }

    }


    @FXML
    void deleteAction(ActionEvent event) {
        employee emp = tableView.getSelectionModel().getSelectedItem();
        ObservableList<employee> emp_List = tableView.getItems();
        emp_List.remove(emp);
        int pk = emp.getEmpID();
        removeEmployee(pk);
    }

    @FXML
    void editAction(ActionEvent event) {

        String firstName = firstText.getText();
        String middleName = middleText.getText();
        String lastName = lastText.getText();
        String email = emailText.getText();
        String add = addText.getText();
        String add2 = add2Text.getText();


        employee emp = tableView.getSelectionModel().getSelectedItem();
        int pk = emp.getEmpID();
        updateEmployee(pk, firstName, middleName, lastName, add, add2, email);

    }

    public void updateEmployee(int pk, String firstName, String middleName, String lastName, String add, String add2, String email) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            String sql = "UPDATE Employee SET EmpFName=?, EmpMName=?, EmpLName=?, EmpAddress1=?, EmpAddress2=?, EmpEmail=? WHERE EmpID=?";

            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, firstName);
            stmt.setString(2, middleName);
            stmt.setString(3, lastName);
            stmt.setString(4, add);
            stmt.setString(5, add2);
            stmt.setString(6, email);
            stmt.setInt(7, pk);
            stmt.executeUpdate();
            tableView.getItems().clear();
            LoadEmployees();
        } catch (Exception e) {
            System.out.println("opps");
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void searchAction(ActionEvent event) {

        }


    public static employee searchAction(String empId) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM employee WHERE EmpID=" + empId;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = TestConnection.dbExecuteQuery(selectStmt);

            //Send ResultSet to the getEmployeeFromResultSet method and get employee object
            employee employee = getEmployeeFromResultSet(rsEmp);
            System.out.println(employee.getEmpFName());

            //Return employee object
            return employee;
        } catch (SQLException e) {
            System.out.println("While searching an employee with " + empId + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    //Auto generates an ID
    public int getPrimaryKey() {
        boolean keyNotFound = true;
        ArrayList<Integer> listOfKeys = new ArrayList<Integer>();
        try {
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT  * FROM Employee");

            while (rs.next()) {
                int empID = rs.getInt("EmpID");
                listOfKeys.add(new Integer(empID));
            }

            while (keyNotFound) {
                int value = (int) (0 + Math.random() * 200);
                Integer key = new Integer(value);

                if (!listOfKeys.contains(key)) {
                    keyNotFound = false;
                    return value;

                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to add employee...");
        }
        return -1;
    }

    //Remove Employee
    public void removeEmployee(int pk) {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            stmt.execute("DELETE FROM Employee WHERE EmpID=" + Integer.toString(pk));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Unable to delete employee...");
        }

    }


    //Use ResultSet from DB as parameter and set Employee Object's attributes and return employee object.
    private static employee getEmployeeFromResultSet(ResultSet rs) throws SQLException {
        employee emp = null;
        if (rs.next()) {
            int empID = rs.getInt("EmpID");
            String firstName = rs.getString("EmpFName");
            String middleName = rs.getString("EmpMName");
            String lastName = rs.getString("EmpLName");
            Date dateBDay = rs.getDate("EmpBdate");
            String address1 = rs.getString("EmpAddress1");
            String address2 = rs.getString("EmpAddress2");
            String email = rs.getString("EmpEmail");
            emp = new employee(empID, firstName, middleName, lastName, dateBDay, address1, address2, email);
        }
        return emp;
    }


    private void LoadEmployees() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;" +
                    "databaseName=PhoenixSolutions;integratedSecurity=true;";
            Connection conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Employee");


            while (rs.next()) {
                employee emp;
                int empID = rs.getInt("EmpID");
                String firstName = rs.getString("EmpFName");
                String middleName = rs.getString("EmpMName");
                String lastName = rs.getString("EmpLName");
                Date dateBDay = rs.getDate("EmpBdate");
                String address1 = rs.getString("EmpAddress1");
                String address2 = rs.getString("EmpAddress2");
                String email = rs.getString("EmpEmail");
                emp = new employee(empID, firstName, middleName, lastName, dateBDay, address1, address2, email);
                //Add employee to the ObservableList

                tableView.getItems().addAll(emp);
            }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Employees couldn't be loaded...");
        }
        tableView.setOnMouseClicked((javafx.scene.input.MouseEvent event) -> {
            employee emp = tableView.getSelectionModel().getSelectedItem();
            String firstName = emp.getEmpFName();
            String middleName = emp.getEmpMName();
            String lastName = emp.getEmpLName();
            String add = emp.getEmpAddress1();
            String add2 = emp.getEmpAddress2();
            String email = emp.getEmpEmail();

            firstText.setText(firstName);
            middleText.setText(middleName);
            lastText.setText(lastName);
            addText.setText(add);
            add2Text.setText(add2);
            emailText.setText(email);
        });

    }

    //Clear Text Field
    public void clearAction(ActionEvent event) {
        firstText.setText("");
        middleText.setText("");
        lastText.setText("");
        bdateText.setValue(null);
        addText.setText("");
        add2Text.setText("");
        emailText.setText("");
    }
}
