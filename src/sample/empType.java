package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class empType{

    private int EmpTypeID;
    private SimpleStringProperty EmpType;
    private SimpleStringProperty EmpTypeDesc;


    public empType(Integer EmpTypeID, String EmpType, String EmpTypeDesc)
    {
        this.EmpTypeID = EmpTypeID;
        this.EmpType = new SimpleStringProperty(EmpType);
        this.EmpTypeDesc = new SimpleStringProperty(EmpTypeDesc);

    }

    public Integer getEmpTypeID() {
        return EmpTypeID;
    }

    public void setEmpTypeID(Integer EmpTypeID) {
        this.EmpTypeID = EmpTypeID;
    }

    public String getEmpType() {
        return EmpType.get();
    }

    public StringProperty TypeProperty() {
        return EmpType;
    }

    public void setEmpType(String EmpType) {
        this.EmpType.set(EmpType);
    }

    public String getEmpTypeDesc() {
        return EmpTypeDesc.get();
    }

    public StringProperty DescrProperty() {
        return EmpTypeDesc;
    }

    public void setEmpTypeDesc(String EmpTypeDesc) {
        this.EmpTypeDesc.set(EmpTypeDesc);
    }
}
